import React from 'react';

class Inventory extends React.Component {

	constructor() {
		super();

		this.state = {
			inventory : [{
				name : "Choco Butternut",
				stock : 20,
				price : 35
			}],
			cart : [],
			flag : '',
			alert : []

		}

		this.addToCart = this.addToCart.bind(this)
		this.updateView = this.updateView.bind(this)
		this.removeFromCart = this.removeFromCart.bind(this)
		this.addInventory = this.addInventory.bind(this)

	}

	addInventory() {

		{/*Get Current copy of inventory*/}
		var my_array = this.state.inventory
		{/*Get User Inputs*/}
		var nItem = document.getElementById('new_item').value
		var nPrice = document.getElementById('new_price').value
		var nStock = document.getElementById('new_qty').value

		var checknItem = nItem.search(/[a-zA-z]/)
		var checknPrice = nItem.search(/[0-9]/)
		var checknStock = nItem.search(/[0-9]/)

		{/*Validate data fields*/}
		if (nItem == '') {
			this.state.alert.push(alert("Please input Item Name."))
			return
		} else if (nPrice == '') {
			this.state.alert.push(alert("Please input Item Price."))
			return

		} else if (nStock == '') {
			this.state.alert.push(alert("Please input Item Quantity."))
			return
		} else if (checknPrice == 0 || nPrice == 0) {
			this.state.alert.push(alert("Invalid data input for Price."))
			return
		} else if (checknStock == 0 || nStock == 0) {
			this.state.alert.push(alert("Invalid data input for Stock."))
			return
		}

		{/*Start push to my_array*/}
		var hash_cont = {}
		hash_cont.name = nItem
		hash_cont.price = nPrice
		hash_cont.stock = nStock

		my_array.push(hash_cont)

		{/*Set new inventory data*/}
		this.setState({inventory : my_array})

	}

	removeFromCart(item) {

		{/*Get Item details before removing from cart*/}
		var return_stock = 0
		var temp_array = this.state.cart
		for (var x = 0; x < this.state.cart.length; x++) {
			if (this.state.cart[x].name == item) {
				return_stock = this.state.cart[x].qty
				temp_array.splice(x,1)
				this.setState({cart : temp_array})
			}
		}

		{/*Return the qty to stock*/}
		for (var x = 0; x < this.state.inventory.length; x++) {
			if (this.state.inventory[x].name == item) {
				var re_stock = this.state.inventory[x].stock
				re_stock = parseInt(re_stock) + parseInt(return_stock)
				this.state.inventory[x].stock = re_stock
			}
		}
	}

	updateView(view) {

		this.setState({flag : view})
	}

	addToCart(item) {

		var count = document.getElementById(item).value;
		var temp_hash = {}

		for (var x = 0 ; x < this.state.inventory.length; x++) {

			if (this.state.inventory[x].name == item) {

				var remaining_stock = this.state.inventory[x].stock - count;

				{/*This block of code is to append items to cart*/}
				temp_hash.name = item
				temp_hash.qty = count
				temp_hash.price = this.state.inventory[x].price
				this.state.cart.push(temp_hash)

				{/*This block of code updates the item in inventory*/}
				this.state.inventory[x].stock = remaining_stock
				
				{/*Tell React to pull new data*/}
				this.setState({flag : 'true'})

				return
			}
		}

	}

	render() {

		if (this.state.flag == 'cart') {

			var rowList = [];
			var sub_total = 0;
			for (var x = 0; x < this.state.cart.length; x++) {

				var item_total = this.state.cart[x].price * this.state.cart[x].qty
				sub_total = item_total + sub_total

				rowList.push(<tr>
					<td>{this.state.cart[x].name}</td>
					<td>{this.state.cart[x].qty}</td>
					<td>{this.state.cart[x].price}</td>
					<td><button onClick={this.removeFromCart.bind(this,this.state.cart[x].name)} >Remove</button></td>
				</tr>);
			}
			return (
				<div>
					<h2> CART LIST </h2>
					<table>
						<tbody>
							<tr> 
								<td>Item Name</td>
								<td>Quantity</td>
								<td>Price</td>
							</tr>
							{rowList}
							<tr>
								<td> Sub Total : </td>
								<td>{sub_total}</td>
							</tr>
						</tbody>
					</table>
					<button onClick={this.updateView.bind(this,null)}>Inventory</button>
				</div>
			);
		} else if (this.state.flag == 'addinv') {

			return (
				<div>
					<h3> ADD NEW ITEM </h3>
					<table>
						<tbody>
							<tr>
								<td> Item Name: </td>
								<td><input type='text' id='new_item' /></td>
							</tr>
							<tr>
								<td> Item Price: </td>
								<td><input type='text' id='new_price' /></td>
							</tr>
							<tr>
								<td> Item Quantity: </td>
								<td><input type='text' id='new_qty' /></td>
							</tr>
							<tr>
								<td><button onClick={this.addInventory}> Add Item </button></td>
								{this.state.alert}
							</tr>
						</tbody>
					</table>
					<button onClick={this.updateView.bind(this,null)}>Inventory</button>
				</div>
			);

		} else {
			{/*Get Inventory List*/}
			var rowList = []
			for (var x = 0 ; x < this.state.inventory.length ; x++) {
				rowList.push(<tr>
					<td>{this.state.inventory[x].name}</td>
					<td>{this.state.inventory[x].stock}</td>
					<td>{this.state.inventory[x].price}</td>
					<td>
						<input type="text" id={this.state.inventory[x].name} />
						<button onClick={this.addToCart.bind(this,this.state.inventory[x].name)}>Add to Cart</button>
					</td>
				</tr>);
			}

			return(
	
				<div>
					<h1> INVENTORY LIST </h1>
					<table>
						<tbody>
						<tr>
							<td> Item Name </td>
							<td> Item Stock </td>
							<td> Item Price </td>
						</tr>
						{rowList}
						</tbody>
					</table>
					<button id='cart-btn' onClick={this.updateView.bind(this,'cart')} >View Cart</button>
					<button id='add-inv-btn' onClick={this.updateView.bind(this,'addinv')} > Add Inventory </button>
				</div>
	
			);
		}

	}

}

class Cart extends React.Component {

	render() {

		return (
			<div>
				<h1> CART LIST </h1>
			</div>
		);

	}

}

export default Inventory;
